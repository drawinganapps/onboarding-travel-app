import 'package:flutter/material.dart';
import 'package:on_boarding_app/helper/color_helper.dart';

class UnselectedWidget extends StatelessWidget {
  final bool isSelected;

  const UnselectedWidget({Key? key, required this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Decoration unselectedDecoration = BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: ColorHelper.primary, width: 1));

    final Decoration selectedDecoration = BoxDecoration(
        shape: BoxShape.circle,
        color: ColorHelper.primary,
        border: Border.all(color: ColorHelper.primary, width: 1));

    return Container(
      padding: const EdgeInsets.all(6),
      decoration: isSelected ? selectedDecoration : unselectedDecoration,
    );
  }
}
