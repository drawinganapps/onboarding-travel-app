import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:on_boarding_app/data/dummy_data.dart';
import 'package:on_boarding_app/helper/color_helper.dart';
import 'package:on_boarding_app/routes/AppRoutes.dart';
import 'package:on_boarding_app/widgets/unselected_widget.dart';

class OnBoarding3Screen extends StatelessWidget {
  const OnBoarding3Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/img/cover6.jpg'), fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onPanUpdate: (details) {
                if (details.delta.dx > 0) {
                  Get.toNamed(AppRoutes.HOME2);
                }
              },
              child: Container(
                width: screenWidth,
                height: screenHeight * 0.35,
                padding: const EdgeInsets.only(left: 25, right: 25, top: 25),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Image.asset('assets/icons/cutlery.png',
                        width: 80, height: 80),
                    Text('Foods',
                        style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                            color: ColorHelper.primary)),
                    Container(
                      margin: const EdgeInsets.only(top: 5, bottom: 5),
                      child: Text(DummyData.text3,
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Colors.black, fontSize: 18)),
                    ),
                    Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('skip', style: TextStyle(
                              color: ColorHelper.white
                            )),
                            SizedBox(
                              width: 80,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: const [
                                  UnselectedWidget(isSelected: false),
                                  UnselectedWidget(isSelected: false),
                                  UnselectedWidget(isSelected: true),
                                ],
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: ColorHelper.primary,
                                  borderRadius: BorderRadius.circular(15)
                              ),
                              child: IconButton(
                                onPressed: () {
                                  Get.toNamed(AppRoutes.HOME);
                                },
                                icon: Icon(Icons.navigate_next,
                                    color: ColorHelper.white, size: 25),
                              ),
                            )
                          ],
                        ))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
