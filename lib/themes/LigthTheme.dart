import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:on_boarding_app/helper/color_helper.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: ColorHelper.whiteDarker,
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
  textTheme: GoogleFonts.latoTextTheme().copyWith(
    bodyText1: GoogleFonts.notoSerif(),
  ),
);
