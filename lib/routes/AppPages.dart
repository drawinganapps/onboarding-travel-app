import 'package:get/get.dart';
import 'package:on_boarding_app/screens/on_boarding_2_screen.dart';
import 'package:on_boarding_app/screens/on_boarding_3_screen.dart';
import 'package:on_boarding_app/screens/on_boarding_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.HOME,
        page: () => const OnBoardingScreen(),
        transition: Transition.circularReveal, transitionDuration: Duration(milliseconds: 500)),
    GetPage(
        name: AppRoutes.HOME2,
        page: () => const OnBoarding2Screen(),
        transition: Transition.circularReveal, transitionDuration: Duration(milliseconds: 500)),
    GetPage(
        name: AppRoutes.HOME3,
        page: () => const OnBoarding3Screen(),
        transition: Transition.circularReveal, transitionDuration: Duration(milliseconds: 500)),
  ];
}
