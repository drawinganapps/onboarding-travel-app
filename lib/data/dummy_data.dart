class DummyData {
  static String text1 = "Embrace different cultures with us to experience what it's like to be a part of a community other than your own.";
  static String text2 = "Spend time in the nature to reduce stress, improve health, increase focus, boost your creativity!";
  static String text3 = "Experience how the street food is made in front of you. You can see exactly how your food is served.";
}